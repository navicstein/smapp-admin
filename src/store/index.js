import Vue from "vue";
import Vuex from "vuex";

import principal from "./modules/principal";
import school from "./modules/school";
import VuexPersistence from "vuex-persist";

const vuexLocal = new VuexPersistence({
  key: "smapp-admin",
  storage: window.localStorage
});

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  modules: {
    principal,
    school
  },
  strict: debug,
  mutations: {},
  actions: {},
  plugins: [vuexLocal.plugin]
});
