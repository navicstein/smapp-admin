import router from "../../router";
const state = {
    principals: [], // All principals
    principal: {}, // single current principal {.. school}
    staff: [] // All staff the principal have
  },
  mutations = {
    // Set ALl principals
    setPrincipals(state, principals) {
      state.principals = principals;
    },
    // Set one principal including its school, and staff
    setPrincipal(state, principal) {
      state.principal = principal;
    },
    setStaff(state, staff) {
      state.staff = staff;
    }
  },
  actions = {
    async loadUser({ commit, state }, id) {
      var { data } = await axios.get("/user/" + id);
      commit("setPrincipal", data);
      router.push("/staff/" + data.id);
    },
    async loadPrincipal({ commit, state }, id) {
      var { data } = await axios.get("/user/" + id);
      commit("setPrincipal", data);
      axios
        .get("/user/", {
          params: {
            principal: id
          }
        })
        .then(({ data }) => {
          commit("setStaff", data);
          router.push(
            "/principals/" +
              Math.random()
                .toString(36)
                .substring(5)
          );
        });
    },
    getPrincipals({ commit, state }) {
      if (state.principals.length === 0) {
        axios.get("/principal/get-principals").then(({ data }) => {
          commit("setPrincipals", data);
        });
      } else {
        console.log(
          "Skipping request for getting principals, list is upto date"
        );
      }
    },

    deletePrincipal({ state, commit }, principal) {
      var pr = state.principals.find(z => z.id === principal);
      if (pr) {
        axios
          .delete("/principals/delete-principal", { principal: pr.id })
          .then(({ data }) => {
            state.principal.splice(pr, 1);
            console.log("Deleted Principal", pr);
          });
      }
    }
  },
  getters = {
    totalNoOfPrincipals(state) {
      return state.principals.length;
    }
  };

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
