import router from "../../router";
const state = {
  schools: [],
  school: {}, // current school
  classes: [] // current schools classes
};
const mutations = {
  SET_SCHOOLS(state, schools) {
    state.schools = schools;
  },
  SET_SCHOOL(state, school) {
    state.school = school;
  },
  SET_CLASSES(state, classes) {
    state.classes = classes;
  }
};
const actions = {
  async viewSchool({ state, commit }, id) {
    var { data } = await axios.get("/class/", {
      params: {
        school: id
      }
    });
    commit("SET_CLASSES", data);
    console.log(data);
    axios.get("/school/" + id).then(({ data }) => {
      commit("SET_SCHOOL", data);
      router.push("/schools/" + data.id);
    });
  },
  getAllSchools({ state, commit }) {
    if (state.schools.length === 0) {
      axios.get("/school").then(({ data }) => {
        commit("SET_SCHOOLS", data);
      });
    } else {
      console.log("Skipping request for getting schools, list is upto date");
    }
  }
};
const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
